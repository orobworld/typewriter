# Typewriter

This repository contains a Java program that uses CPLEX to optimize the layout of a one-dimensional typewriter keyboard. See the blog post ["Optimizing 19th Century Typewriters"](https://nathanbrixius.wordpress.com/2018/11/26/optimizing-19th-century-typewriters/) for details of the problem.

The program requires two external libraries:

* CPLEX (tested with version 12.8); and
* Apache Commons CLI 1.4 (for processing command line options).

The Apache library is open-source.

For a list of options, run the program with either ``-h`` or ``--help`` in the command line.

The program requires a data file containing the number $`n`$ of symbols to be placed on the keyboard (for example, 26 for the lower case English alphabet) and a square matrix of dimension $`n \times n`$ whose $`i,j`$ entry contains the frequency with which symbol $`j`$ immediately follows symbol $`i`$. The file `cro26_data.txt` is a sample data file, based on the content of the blog post by Nathan Brixius cited above, using data he generously provided.
