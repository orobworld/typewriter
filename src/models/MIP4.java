package models;

import ilog.concert.IloException;
import ilog.concert.IloLinearNumExpr;
import ilog.concert.IloNumVar;
import java.util.ArrayList;
import java.util.stream.IntStream;
import typewriter.Alphabet;
import static typewriter.ProgramOptions.DISTANCECUTS;
import static typewriter.ProgramOptions.RPCUT;

/**
 * MIP4 adds to MIP1 additional constraints on the objective contribution from
 * each symbol.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class MIP4 extends MIP {
  // Additional CPLEX objects
  private final IloNumVar[] position;     // character positions
  private final IloNumVar[][] distance;   // distances between character pairs
  private final IloNumVar[] travel;       // objective contribution of each
                                          // symbol

  /**
   * Constructor.
   * @param alpha the problem instance
   * @param opts the options vector
   * @throws IloException if CPLEX balks at any step of model construction
   */
  public MIP4(final Alphabet alpha, final Object[] opts)
         throws IloException {
    super(alpha, opts);
    // Create the variables not created in the parent class.
    position = new IloNumVar[size];
    travel = new IloNumVar[size];
    for (int i = 0; i < size; i++) {
      position[i] = mip.numVar(0, size - 1, "pos_" + i);
        // position[i] is the position of character i on the keyboard
      travel[i] = mip.numVar(0, Double.MAX_VALUE, "travel_" + i);
        // travel[i] is the overall objective contribution from the placement
        // of symbol i
    }
    distance = new IloNumVar[size][size];
    for (int i = 0; i < size; i++) {
      for (int j = 0; j < size; j++) {
        distance[i][j] = mip.numVar(0, size - 1, "dist_" + i + "_" + j);
          // distance[i][j] is the distance between characters i and j on the
          // keyboard
      }
    }
    // Create the objective function (minimize the overall travel).
    mip.addMinimize(mip.sum(travel));
    // Determine each character's position from its assignment.
    IloLinearNumExpr expr;
    for (int i = 0; i < size; i++) {
      expr = mip.linearNumExpr();
      for (int j = 1; j < size; j++) {
        expr.addTerm(j, assign[i][j]);
      }
      mip.addEq(expr, position[i], "position_" + i);
    }
    // Define the distance between every pair of characters as the absolute
    // value of the difference in their positions.
    for (int i = 0; i < size; i++) {
      for (int j = 0; j < size; j++) {
        mip.addGe(distance[i][j], mip.diff(position[i], position[j]),
                  "define_dist_" + i + "_" + j + "_1");
        mip.addGe(distance[i][j], mip.diff(position[j], position[i]),
                  "define_dist_" + i + "_" + j + "_2");
      }
    }
    // Exploit symmetry of the distance matrix to allow the presolver to
    // eliminate about half the distance variables.
    for (int i = 0; i < size; i++) {
      for (int j = 0; j < i; j++) {
        mip.addEq(distance[i][j], distance[j][i], "dsymm_" + i + "_" + j);
      }
    }
    // Define the objective contributions in terms of the distances.
    for (int i = 0; i < size; i++) {
      mip.addEq(travel[i],
                mip.scalProd(alphabet.getFrequencies(i), distance[i]),
                "define_travel_" + i);
    }
    // Add lower bounds on the travel from each symbol, charitably assuming
    // that the frequency with which a symbol follows this one and the distance
    // at which it is placed are inversely related.
    for (int i = 0; i < size; i++) {
      // Get the sorted transition frequencies for all symbols other than i.
      int[] freq = alphabet.getSortedFrequencies(i);
      for (int j = 0; j < size; j++) {
        mip.addGe(travel[i],
                  mip.prod(dot(freq, getSortedDistances(j)), assign[i][j]),
                  "travel_" + i + "_" + j + "_lb");
      }
    }
    // Add a cut suggested by Rob Pratt in a blog comment?
    if ((boolean) opts[RPCUT.ordinal()]) {
      expr = mip.linearNumExpr();
      for (int i = 0; i < size; i++) {
        for (int j = i + 1; j < size; j++) {
          expr.addTerm(1.0, distance[i][j]);
        }
      }
      mip.addEq(expr, alphabet.distanceSum(), "RobP_cut");
    }
    // Add redundant constraints regarding the sum of distances from any
    // symbol to all other symbols?
    if ((boolean) opts[DISTANCECUTS.ordinal()]) {
      for (int i = 0; i < size; i++) {
        expr = mip.linearNumExpr();
        for (int j = 0; j < size; j++) {
          expr.addTerm(alphabet.sumDistances(j), assign[i][j]);
          expr.addTerm(-1.0, distance[i][j]);
        }
        mip.addEq(expr, 0, "Distance_sum_" + i);
      }
    }
    mip.exportModel("/tmp/mip4.lp");
  }

  /**
   * Take the inner product of two integer vectors (assumed to have equal
   * length).
   * @param a the first vector
   * @param b the second vector
   * @return the inner product
   */
  private int dot(final int[] a, final int[] b) {
    return IntStream.range(0, a.length)
                    .parallel()
                    .map((i) -> a[i] * b[i])
                    .reduce(0, Integer::sum);
  }

  /**
   * Get a list of the distances from any position to all other positions,
   * sorted into ascending order.
   * @param pos the starting position
   * @return an increasing list of distances to all other positions
   */
  private int[] getSortedDistances(final int pos) {
    ArrayList<Integer> d = new ArrayList<>();
    for (int j = 1; j <= pos; j++) {
      d.add(j);
    }
    for (int j = 1; j < size - pos; j++) {
      d.add(j);
    }
    return d.stream().sorted().mapToInt((i) -> i).toArray();
  }

  /**
   * Get a vector indicating the position of each symbol on the keyboard.
   * @return a vector of symbol positions
   * @throws IloException if a CPLEX error occurs
   */
  @Override
  protected int[] getPositions() throws IloException {
    int[] pos = new int[size];
    double[] p = mip.getValues(position);
    for (int i = 0; i < size; i++) {
      pos[i] = (int) Math.round(p[i]);
    }
    return pos;
  }
}
