package models;

import ilog.concert.IloException;
import ilog.concert.IloLinearNumExpr;
import ilog.concert.IloNumVar;
import typewriter.Alphabet;
import static typewriter.ProgramOptions.DISTANCECUTS;
import static typewriter.ProgramOptions.RPCUT;

/**
 * MIP1 is a mixed integer programming model for the typewriter problem.
 * It uses explicit variables for the position of each character and the
 * distance between characters.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class MIP1 extends MIP {
  // Additional CPLEX objects
  private final IloNumVar[] position;     // character positions
  private final IloNumVar[][] distance;   // distances between character pairs

  /**
   * Constructor.
   * @param alpha the problem instance
   * @param opts the options vector
   * @throws IloException if CPLEX balks at any step of model construction
   */
  public MIP1(final Alphabet alpha, final Object[] opts)
         throws IloException {
    super(alpha, opts);
    // Create the variables not created in the parent class.
    position = new IloNumVar[size];
    for (int i = 0; i < size; i++) {
      position[i] = mip.numVar(0, size - 1, "pos_" + i);
        // position[i] is the position of character i on the keyboard
    }
    distance = new IloNumVar[size][size];
    for (int i = 0; i < size; i++) {
      for (int j = 0; j < size; j++) {
        distance[i][j] = mip.numVar(0, size - 1, "dist_" + i + "_" + j);
          // distance[i][j] is the distance between characters i and j on the
          // keyboard
      }
    }
    // Create the objective (minimizing the frequency-weighted total distance
    // among consecutive characters.
    IloLinearNumExpr expr = mip.linearNumExpr();
    for (int i = 0; i < size; i++) {
      for (int j = 0; j < size; j++) {
        expr.addTerm(alpha.getFrequency(i, j), distance[i][j]);
      }
    }
    mip.addMinimize(expr);
    // Determine each character's position from its assignment.
    for (int i = 0; i < size; i++) {
      expr = mip.linearNumExpr();
      for (int j = 1; j < size; j++) {
        expr.addTerm(j, assign[i][j]);
      }
      mip.addEq(expr, position[i], "position_" + i);
    }
    // Define the distance between every pair of characters as the absolute
    // value of the difference in their positions.
    for (int i = 0; i < size; i++) {
      for (int j = 0; j < size; j++) {
        mip.addGe(distance[i][j], mip.diff(position[i], position[j]),
                  "define_dist_" + i + "_" + j + "_1");
        mip.addGe(distance[i][j], mip.diff(position[j], position[i]),
                  "define_dist_" + i + "_" + j + "_2");
      }
    }
    // Exploit symmetry of the distance matrix to allow the presolver to
    // eliminate about half the distance variables.
    for (int i = 0; i < size; i++) {
      for (int j = 0; j < i; j++) {
        mip.addEq(distance[i][j], distance[j][i], "dsymm_" + i + "_" + j);
      }
    }
    // Add a cut suggested by Rob Pratt in a blog comment?
    if ((boolean) opts[RPCUT.ordinal()]) {
      expr = mip.linearNumExpr();
      for (int i = 0; i < size; i++) {
        for (int j = i + 1; j < size; j++) {
          expr.addTerm(1.0, distance[i][j]);
        }
      }
      mip.addEq(expr, alphabet.distanceSum(), "RobP_cut");
    }
    // Add redundant constraints regarding the sum of distances from any
    // symbol to all other symbols?
    if ((boolean) opts[DISTANCECUTS.ordinal()]) {
      for (int i = 0; i < size; i++) {
        expr = mip.linearNumExpr();
        for (int j = 0; j < size; j++) {
          expr.addTerm(alphabet.sumDistances(j), assign[i][j]);
          expr.addTerm(-1.0, distance[i][j]);
        }
        mip.addEq(expr, 0, "Distance_sum_" + i);
      }
    }
  }

  /**
   * Get a vector indicating the position of each symbol on the keyboard.
   * @return a vector of symbol positions
   * @throws IloException if a CPLEX error occurs
   */
  @Override
  protected int[] getPositions() throws IloException {
    int[] pos = new int[size];
    double[] p = mip.getValues(position);
    for (int i = 0; i < size; i++) {
      pos[i] = (int) Math.round(p[i]);
    }
    return pos;
  }

}
