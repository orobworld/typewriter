package models;

import ilog.concert.IloException;
import ilog.concert.IloIntVar;
import ilog.concert.IloLinearIntExpr;
import ilog.concert.cppimpl.IloAlgorithm.Status;
import ilog.cp.IloCP;
import java.util.Arrays;
import java.util.Optional;
import typewriter.Alphabet;
import typewriter.ProgramOptions;

/**
 * CP creates a constraint programming model for the keyboard problem.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class CP implements Model {
  private final int size;                 // alphabet size
  private final Alphabet alphabet;        // the problem data

  // CP objects
  private final IloCP cp;                // the CP model object
  private final IloIntVar[] position;    // position of each symbol
  private final IloIntVar[] symbol;      // symbol at each position
  private final IloIntVar[][] distance;  // distance between any pair of symbols

  /**
   * Constructor.
   * @param alpha the problem data
   * @throws IloException if CP Optimizer balks at building the model
   */
  public CP(final Alphabet alpha) throws IloException {
    // Get the data.
    alphabet = alpha;
    size = alphabet.getSize();
    // Create the model object.
    cp = new IloCP();
    // Create the variables.
    position = new IloIntVar[size];
    symbol = new IloIntVar[size];
    distance = new IloIntVar[size][size];
    for (int i = 0; i < size; i++) {
      position[i] = cp.intVar(0, size - 1, "position_of_" + i);
      symbol[i] = cp.intVar(0, size - 1, "symbol_at_" + i);
      for (int j = 0; j < size; j++) {
        distance[i][j] = cp.intVar(0, size - 1, "distance_" + i + "_" + j);
      }
    }
    // Constrain symbol and position to be permutations.
    cp.add(cp.allDiff(symbol));
    cp.add(cp.allDiff(position));
    // Flag position and symbol as inversely related.
    cp.add(cp.inverse(position, symbol));
    // Define distances.
    for (int i = 0; i < size; i++) {
      for (int j = 0; j < size; j++) {
        if (i <= j) {
          cp.addEq(distance[i][j], cp.abs(cp.diff(position[i], position[j])));
        } else {
          cp.addEq(distance[i][j], distance[j][i]);
        }
      }
    }
    // Mitigate symmetry by putting the most popular symbol on the left side
    // of the keyboard.
    int mid = Math.floorDiv(size + 1, 2) - 1;  // middle position
    int ix = alphabet.mostFrequent();
    position[ix].setUB(mid);
    // Add the objective function.
    IloLinearIntExpr expr = cp.linearIntExpr();
    for (int i = 0; i < size; i++) {
      for (int j = 0; j < size; j++) {
        expr.addTerm(alphabet.getFrequency(i, j), distance[i][j]);
      }
    }
    cp.addMinimize(expr);
  }

  /**
   * Solve the model.
   * @param opts the program options
   * @return the final solver status
   * @throws IloException if the optimizer blows up
   */
  @Override
  public Status solve(final Object[] opts)
                throws IloException {
    // Set the time limit if specified.
    Optional<Double> tl =
      (Optional<Double>) opts[ProgramOptions.TIMELIMIT.ordinal()];
    if (tl.isPresent()) {
      cp.setParameter(IloCP.DoubleParam.TimeLimit, tl.get());
    }
    // Set the log intensity if specified.
    Optional<Integer> log =
      (Optional<Integer>) opts[ProgramOptions.LOG.ordinal()];
    if (log.isPresent()) {
      cp.setParameter(IloCP.IntParam.LogPeriod, log.get());
    } else {
      // If no log interval is specified, switch to terse mode.
      cp.setParameter(IloCP.IntParam.LogVerbosity, IloCP.ParameterValues.Terse);
    }
    cp.solve();
    return cp.getStatus();
  }

  /**
   * Does the solver have a feasible solution?
   * @return true if a solution was found
   */
  private boolean hasSolution() {
    return cp.getStatus() == Status.Feasible
           || cp.getStatus() == Status.Optimal;
  }

  /**
   * Fetch the solution if one exists.
   * @return the current layout (null if none was found)
   * @throws IloException if CP Optimizer cannot find a solution
   */
  @Override
  public int[] getSolution() throws IloException {
    if (hasSolution()) {
      double[] vals = new double[size];
      cp.getValues(symbol, vals);
      int[] pos = new int[size];
      for (int i = 0; i < size; i++) {
        pos[i] = (int) Math.round(vals[i]);
      }
      return pos;
    } else {
      return null;
    }
  }

  /**
   * Generate a solution report.
   * @return the solution report
   */
  @Override
  public String reportSolution() {
    StringBuilder sb = new StringBuilder();
    try {
      if (hasSolution()) {
        sb.append("Reported objective value = ").append(cp.getObjValue())
          .append("\n\nBest known layout:\n[");
        int[] layout = getSolution();
        if (alphabet.isCharacter()) {
          Arrays.stream(layout)
                .forEach((i) -> sb.append(" ").append(alphabet.toChar(i)));
        } else {
          Arrays.stream(layout)
                .forEach((i) -> sb.append(" ").append(i));
        }
        sb.append(" ]\n");
      } else {
        sb.append("No solution is available!");
      }
    } catch (IloException ex) {
      return "Unable to process the solution due to an error:\n"
             + ex.getMessage();
    }
    return sb.toString();
  }

  /**
   * Set an initial incumbent.
   * This is not (currently) supported for the CP model.
   * @param solution the incumbent to set
   * @throws IloException if CPLEX refuses to accept it
   */
  @Override
  public void setIncumbent(final int[] solution) throws IloException {
  }
}
