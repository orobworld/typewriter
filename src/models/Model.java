package models;

import ilog.concert.IloException;

/**
 * Model is a common interface for all supported models.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public interface Model {
  /** ModelType enumerates the various supported models. */
  enum ModelType {
    /** MIP1 uses explicit variables for symbol position and distance between
        symbol pairs. */
    MIP1,
    /** MIP2 uses continuous variables for pairwise distances, with the option
        of making the (numerous) defining constraints lazy. */
    MIP2,
    /** MIP3 is based on MIP1, with the position variables declared integer
     *  and branching priorities assigned to them. */
    MIP3,
    /** MIP4 is based on MIP1, with additional constraints to impose lower
     * bounds on the objective contribution from each symbol. */
    MIP4,
    /** QP is an explicit quadratic assignment model. */
    QP,
    /** CP is a constraint programming model. */
    CP
  };

  /**
   * Generate a solution report.
   * @return the solution report
   */
  String reportSolution();

  /**
   * Solve the model.
   * @param opts the options to use
   * @return the final solver status
   * @throws IloException if the solver blows up
   */
  Object solve(final Object[] opts) throws IloException;

  /**
   * Fetch the solution if one exists.
   * @return the current solution (null if none was found)
   * @throws IloException if the solver cannot find a solution it thinks exists
   */
  int[] getSolution() throws IloException;

  /**
   * Set an initial incumbent (if supported by the model type).
   * @param solution the incumbent to set
   * @throws IloException if CPLEX refuses to accept it
   */
  void setIncumbent(final int[] solution) throws IloException;
}
