package models;

import ilog.concert.IloException;
import ilog.concert.IloQuadNumExpr;
import typewriter.Alphabet;

/**
 * QP models the typewriter problem directly as a quadratic integer program.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class QP extends MIP {
  private static final double HALF = 0.5;  // used for rounding binary variables

  /**
   * Constructor.
   * @param alpha the problem instance
   * @param opts the options vector
   * @throws IloException if CPLEX balks at any step of model construction
   */
  public QP(final Alphabet alpha, final Object[] opts) throws IloException {
    super(alpha, opts);
    // Add the objective function.
    IloQuadNumExpr expr = mip.quadNumExpr();
    for (int k = 0; k < size; k++) {
      for (int m = 0; m < size; m++) {
        if (k != m) {
          int dist = Math.abs(k - m);
          for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
              if (i != j) {
                expr.addTerm(alphabet.getFrequency(i, j) * dist,
                             assign[i][k], assign[j][m]);
              }
            }
          }
        }
      }
    }
    mip.addMinimize(expr);
  }

  /**
   * Get a vector indicating the position of each symbol on the keyboard.
   * @return a vector of symbol positions
   * @throws IloException if a CPLEX error occurs
   */
  @Override
  protected int[] getPositions() throws IloException {
    int[] pos = new int[size];
    for (int i = 0; i < size; i++) {
      double[] x = mip.getValues(assign[i]);
      for (int j = 0; j < size; j++) {
        if (x[j] > HALF) {
          pos[i] = j;
          break;
        }
      }
    }
    return pos;
  }
}
