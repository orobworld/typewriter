package models;

import ilog.concert.IloException;
import ilog.concert.IloLinearNumExpr;
import ilog.concert.IloNumVar;
import ilog.concert.IloRange;
import typewriter.Alphabet;
import static typewriter.ProgramOptions.DISTANCECUTS;
import static typewriter.ProgramOptions.LAZY;
import static typewriter.ProgramOptions.RPCUT;

/**
 * MIP2 is a mixed integer programming model for the typewriter problem.
 * It uses a standard linearization of products of binary variables.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class MIP2 extends MIP {
  private static final double HALF = 0.5;  // used for rounding binary variables
  // Additional CPLEX objects
  private final IloNumVar[][] distance;
    // distance[i][j] = distance between symbol i and symbol j

  /**
   * Constructor.
   * @param alpha the problem instance
   * @param opts the options vector
   * @throws IloException if CPLEX balks at any step of model construction
   */
  public MIP2(final Alphabet alpha, final Object[] opts)
         throws IloException {
    super(alpha, opts);
    // Create the variables not created in the parent class.
    distance = new IloNumVar[size][size];
    for (int i = 0; i < size; i++) {
      for (int j = 0; j < size; j++) {
        int max = (i == j) ? 0 : size - 1;
        distance[i][j] = mip.numVar(0, max, "dist_" + i + "_" + j);
      }
    }
    // Create the objective (minimizing the frequency-weighted total distance
    // among consecutive characters.
    IloLinearNumExpr expr = mip.linearNumExpr();
    for (int i = 0; i < size; i++) {
      for (int j = 0; j < size; j++) {
        expr.addTerm(alphabet.getFrequency(i, j), distance[i][j]);
      }
    }
    mip.addMinimize(expr);
    // Define distances.
    for (int i = 0; i < size; i++) {
      for (int j = 0; j < size; j++) {
        if (i != j) {
          for (int m = 0; m < size; m++) {
            for (int n = 0; n < size; n++) {
              if (m != n) {
                int d = Math.abs(m - n);
                expr = mip.linearNumExpr(-d);
                expr.addTerm(d, assign[i][m]);
                expr.addTerm(d, assign[j][n]);
                if ((boolean) opts[LAZY.ordinal()]) {
                  mip.addLazyConstraint(
                    (IloRange) mip.ge(distance[i][j], expr,
                                      "dist_" + i + "_" + j + "_" + m + "_" + n)
                  );
                } else {
                  mip.addGe(distance[i][j], expr, "dist_" + i + "_" + j + "_"
                                                  + m + "_" + n);
                }
              }
            }
          }
        }
      }
    }
    // Exploit symmetry of the distance matrix to allow the presolver to
    // eliminate about half the distance variables.
    for (int i = 0; i < size; i++) {
      for (int j = 0; j < i; j++) {
        mip.addEq(distance[i][j], distance[j][i], "dsymm_" + i + "_" + j);
      }
    }
    // Add a cut suggested by Rob Pratt in a blog comment?
    if ((boolean) opts[RPCUT.ordinal()]) {
      expr = mip.linearNumExpr();
      for (int i = 0; i < size; i++) {
        for (int j = i + 1; j < size; j++) {
          expr.addTerm(1.0, distance[i][j]);
        }
      }
      mip.addEq(expr, alphabet.distanceSum(), "RobP_cut");
    }
    // Add redundant constraints regarding the sum of distances from any
    // symbol to all other symbols?
    if ((boolean) opts[DISTANCECUTS.ordinal()]) {
      for (int i = 0; i < size; i++) {
        expr = mip.linearNumExpr();
        for (int j = 0; j < size; j++) {
          expr.addTerm(alphabet.sumDistances(j), assign[i][j]);
          expr.addTerm(-1.0, distance[i][j]);
        }
        mip.addEq(expr, 0, "Distance_sum_" + i);
      }
    }
  }

  /**
   * Get a vector indicating the position of each symbol on the keyboard.
   * @return a vector of symbol positions
   * @throws IloException if a CPLEX error occurs
   */
  @Override
  protected int[] getPositions() throws IloException {
    int[] pos = new int[size];
    for (int i = 0; i < size; i++) {
      double[] x = mip.getValues(assign[i]);
      for (int j = 0; j < size; j++) {
        if (x[j] > HALF) {
          pos[i] = j;
          break;
        }
      }
    }
    return pos;
  }
}
