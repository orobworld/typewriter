package models;

import ilog.concert.IloException;
import ilog.concert.IloLinearNumExpr;
import ilog.concert.IloNumVar;
import ilog.cplex.IloCplex;
import java.util.Arrays;
import java.util.Optional;
import java.util.OptionalDouble;
import typewriter.Alphabet;
import typewriter.ProgramOptions;
import static typewriter.ProgramOptions.PRIORITY;
import static typewriter.ProgramOptions.SOS;

/**
 * MIP is the parent class for MIP models.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public abstract class MIP implements Model {
  // Fields common to all MIP models.
  protected final int size;                 // alphabet size
  protected final Alphabet alphabet;        // the problem data
  protected int[] layout;                   // the solution (if found)

  // CPLEX objects common to all MIP models.
  protected final IloCplex mip;             // the actual MIP1 model
  protected final IloNumVar[][] assign;     // assignments of chars to positions

  /**
   * Constructor.
   * @param alpha the problem instance
   * @param opts the options vector
   * @throws IloException if CPLEX balks at any step of model construction
   */
  public MIP(final Alphabet alpha, final Object[] opts)
         throws IloException {
    alphabet = alpha;
    size = alpha.getSize();
    // Create the model object.
    mip = new IloCplex();
    // Create the assignment variables.
    assign = new IloNumVar[size][size];
    for (int i = 0; i < size; i++) {
      for (int j = 0; j < size; j++) {
        assign[i][j] = mip.boolVar("assign_" + i + "_to_" + j);
          // assign[i][j] = 1 if character i is assigned to position j
      }
    }
    // Assign every character exactly once.
    for (int i = 0; i < size; i++) {
      mip.addEq(mip.sum(assign[i]), 1.0, "assign_" + i + "_once");
    }
    // Fill every position exactly once.
    IloLinearNumExpr expr;
    for (int j = 0; j < size; j++) {
      expr = mip.linearNumExpr();
      for (int i = 0; i < size; i++) {
        expr.addTerm(1.0, assign[i][j]);
      }
      mip.addEq(expr, 1.0, "fill_" + j + "_once");
    }
    // Since the reverse of any solution has the same objective value, we can
    // arbitrarily require the most frequently used character (breaking ties
    // whimsically) to be in the left half of the keyboard.
    int mid = Math.floorDiv(size + 1, 2) - 1;  // middle position
    int ix = alphabet.mostFrequent();
    for (int j = mid + 1; j < size; j++) {
      assign[ix][j].setUB(0);
    }
    // Add SOS1 constraints (using frequencies) for filling each position?
    if ((boolean) opts[SOS.ordinal()]) {
      for (int j = 0; j < size; j++) {
        IloNumVar[] var = new IloNumVar[size];
        double[] val = new double[size];
        for (int i = 0; i < size; i++) {
          var[i] = assign[i][j];
          val[i] = alphabet.cumulativeFrequency(i);
        }
        mip.addSOS1(var, val, "SOS1_" + j);
      }
    }
    // Assign branching priorities?
    if ((boolean) opts[PRIORITY.ordinal()]) {
      for (int i = 0; i < size; i++) {
        int[] p = new int[size];
        for (int j = 0; j < size; j++) {
          // Assign prioritiy based on frequency (more frequent => higher
          // priority) and position (closer to middle => higher priority).
          p[j] = alphabet.cumulativeFrequency(i) * (Math.min(j, size - j - 1));
        }
        mip.setPriorities(assign[i], p);
      }
    }
  }

  /**
   * Solve the model.
   * @param opts the program options
   * @return the final solver status
   * @throws IloException if the solver blows up
   */
  @Override
  public final IloCplex.Status solve(final Object[] opts) throws IloException {
    // Set the time limit if specified.
    Optional<Double> tl =
      (Optional<Double>) opts[ProgramOptions.TIMELIMIT.ordinal()];
    if (tl.isPresent()) {
      mip.setParam(IloCplex.Param.TimeLimit, tl.get());
    }
    // Set the MIP emphasis if specified.
    Optional<Integer> emphasis =
      (Optional<Integer>) opts[ProgramOptions.EMPHASIS.ordinal()];
    if (emphasis.isPresent()) {
      mip.setParam(IloCplex.Param.Emphasis.MIP, emphasis.get());
    }
     // Set the log intensity if specified.
    Optional<Integer> log =
      (Optional<Integer>) opts[ProgramOptions.LOG.ordinal()];
    if (log.isPresent()) {
      mip.setParam(IloCplex.Param.MIP.Interval, log.get());
    }
    mip.solve();
    return mip.getStatus();
  }

  /**
   * Store the current solution in the layout array.
   * @return true if a solution exists
   * @throws IloException if retrieval of the solution fails
   */
  private boolean decodeSolution() throws IloException {
    if (layout == null && mip.isPrimalFeasible()) {
      layout = new int[size];
      int[] pos = getPositions();
      for (int i = 0; i < size; i++) {
        layout[pos[i]] = i;
      }
      return true;
    } else {
      return layout != null;
    }
  }

  /**
   * Generate a solution report.
   * @return the solution report
   */
  @Override
  public final String reportSolution() {
    StringBuilder sb = new StringBuilder();
    try {
      if (decodeSolution()) {
        sb.append("Reported objective value = ").append(mip.getObjValue())
          .append("\n\nBest known layout:\n[");
        if (alphabet.isCharacter()) {
          Arrays.stream(layout)
                .forEach((i) -> sb.append(" ").append(alphabet.toChar(i)));
        } else {
          Arrays.stream(layout)
                .forEach((i) -> sb.append(" ").append(i));
        }
        sb.append(" ]\n");
      } else {
        sb.append("No solution is available!");
      }
    } catch (IloException ex) {
      return "Unable to process the solution due to an error:\n"
             + ex.getMessage();
    }
    return sb.toString();
  }

  /**
   * Fetch the solution if one exists.
   * @return the current solution (null if none was found)
   * @throws IloException if CPLEX cannot find a solution it thinks exists
   */
  @Override
  public final int[] getSolution() throws IloException {
    if (decodeSolution()) {
      return Arrays.copyOf(layout, size);
    } else {
      return null;
    }
  }

  /**
   * Get a vector indicating the position of each symbol on the keyboard.
   * @return a vector of symbol positions
   * @throws IloException if a CPLEX error occurs
   */
  protected abstract int[] getPositions() throws IloException;

  /**
   * Set an initial incumbent.
   * @param solution the incumbent to set
   * @throws IloException if CPLEX refuses to accept it
   */
  @Override
  public final void setIncumbent(final int[] solution) throws IloException {
    // Create a vector of just the assignments being set to 1.
    double[] val = new double[size];
    IloNumVar[] var = new IloNumVar[size];
    // Fill the arrays by position.
    for (int j = 0; j < size; j++) {
      var[j] = assign[solution[j]][j];
      val[j] = 1.0;
    }
    mip.addMIPStart(var, val, IloCplex.MIPStartEffort.Auto);
  }
}
