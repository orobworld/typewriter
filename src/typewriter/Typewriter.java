package typewriter;

import ilog.concert.IloException;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.NoSuchElementException;
import java.util.Optional;
import models.CP;
import models.MIP1;
import models.MIP2;
import models.MIP3;
import models.MIP4;
import models.Model;
import models.Model.ModelType;
import models.QP;
import org.apache.commons.cli.ParseException;
import static typewriter.ProgramOptions.MODEL;
import static typewriter.ProgramOptions.VERIFY;

/**
 * Typewriter finds the optimal layout for a one-dimensional typewriter
 * keyboard, based on a frequency matrix for consecutive pairs of characters.
 *
 * See https://nathanbrixius.wordpress.com/2018/11/26/optimizing-19th-century
 * -typewriters/ for details of the problem.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class Typewriter {

  /**
   * Null constructor.
   */
  private Typewriter() { }

  /**
   * Read command line parameters, build and solve a MIP model, and output
   * the optimal layout.
   *
   * @param args the command line arguments
   */
  public static void main(final String[] args) {
    // Parse the command line.
    Object[] opts = null;
    try {
      opts = ProgramOptions.parseCommandLine(args);
    } catch (ParseException ex) {
      System.err.println("Could not parse the command line!\n"
                         + ex.getMessage());
      System.exit(1);
    }
    // If the user asked for help (which will already have been printed),
    // just exit.
    if ((boolean) opts[ProgramOptions.HELP.ordinal()]) {
      System.exit(0);
    }
    // Read the data file and create a data object.
    Alphabet alphabet = null;
    try {
      alphabet = new Alphabet(opts);
    } catch (FileNotFoundException ex) {
      System.err.println("Could not find the input file!");
      System.exit(1);
    } catch (InputMismatchException ex) {
      System.err.println("Bumped into a non-integer value in the input file!\n"
                         + ex.getMessage());
      System.exit(1);
    } catch (NoSuchElementException ex) {
      System.err.println("Ran out of data prematurely in the input file:\n"
                         + ex.getMessage());
      System.exit(1);
    }
    // Show the command line.
    System.out.println("Command line options:\n" + String.join(" ", args));
    // Show the data.
    System.out.println("\n" + alphabet);
    // If a candidate solution was provided, verify it.
    int[] test = null;
    Optional<String> start = (Optional<String>) opts[VERIFY.ordinal()];
    if (start.isPresent()) {
      // Get the proposed solution.
      test = parseLayout(start.get(), alphabet);
      // Evaluate the proposed layout.
      try {
        long z = alphabet.valueOf(test);
        System.out.println("The supplied solution is valid with objective"
                           + " value " + z);
      } catch (InvalidSolutionException ex) {
        System.out.println("The proposed solution is not valid:\n"
                           + ex.getMessage());
        test = null;
      }
    }
    // Build the MIP model.
    Model solver = null;
    ModelType model = (ModelType) opts[MODEL.ordinal()];
    try {
      switch (model) {
        case MIP1:
          solver = new MIP1(alphabet, opts);
          break;
        case MIP2:
          solver = new MIP2(alphabet, opts);
          break;
        case QP:
          solver = new QP(alphabet, opts);
          break;
        case MIP3:
          solver = new MIP3(alphabet, opts);
          break;
        case MIP4:
          solver = new MIP4(alphabet, opts);
          break;
        case CP:
          solver = new CP(alphabet);
          break;
        default:
          System.err.println("Unrecognized model type " + model + "!?");
          System.exit(1);
      }
    } catch (IloException ex) {
      System.err.println("Unable to build the MIP model:\n" + ex.getMessage());
      System.exit(1);
    }
    // If a valid solution was entered on the command line, try to use it as
    // an initial incumbent.
    if (test != null) {
      try {
        solver.setIncumbent(test);
      } catch (IloException ex) {
        System.err.println("Unable to use the supplied solution:\n"
                           + ex.getMessage());
        System.exit(1);
      }
    }
    // Solve the MIP model.
    System.out.println("\nSolving the MIP model (version " + model
                       + ") ...\n");
    try {
      Object status = solver.solve(opts);
      // Report the status.
      System.out.println("\nFinal solver status = " + status);
      // Fetch the alleged solution and report on it.
      System.out.println(solver.reportSolution());
      int[] layout = solver.getSolution();
      // If a solution was found, validate it.
      if (layout != null) {
        try {
          System.out.println("The reported solution has a confirmed value of "
                             + alphabet.valueOf(layout));
        } catch (InvalidSolutionException ex) {
          System.err.println("The reported solution is not valid??\n"
                             + ex.getMessage());
        }
      }
    } catch (IloException ex) {
      System.err.println("Unable to solve the model:\n" + ex.getMessage());
      System.exit(1);
    }
  }

  /**
   * Convert a string of either character indices or alphabet symbols,
   * separated by any combination of spaces and commas, into a vector of
   * indices.
   * @param layout the layout string
   * @param alphabet the alphabet being used
   * @return the corresponding integer vector
   */
  private static int[] parseLayout(final String layout,
                                   final Alphabet alphabet) {
    int[] indices;
    char[] chars;
    // First replace any commas with spaces.
    String temp = layout.replaceAll(",", " ");
    // Now split it where white space occur.
    String[] temp2 = temp.split("\\W+");
    // If the first string is an integer, assume that the solution is in
    // the form of indices.
    if (temp2[0].matches("\\d+")) {
      // The solution specifies indices. Parse each string into an int and
      // accumulate them in an array.
      indices = Arrays.stream(temp2)
                      .mapToInt(Integer::parseInt)
                      .toArray();
    } else {
      // Get the anchor character if given.
      // Each string should contain a single character. Convert it to a byte
      // and subtract the anchor.
      indices = Arrays.stream(temp2)
                      .mapToInt((x) -> alphabet.toIndex(x))
                      .toArray();
    }
    // Convert the indices to characters as well.
    chars = new char[indices.length];
    for (int i = 0; i < indices.length; i++) {
      chars[i] = alphabet.toChar(indices[i]);
    }
    // Announce the results.
    System.out.println("\nEvaluation solution with index vector\n"
                       + Arrays.toString(indices)
                       + "\n and equivalent symbol sequence\n"
                       + Arrays.toString(chars));
    return indices;
  }
}
