package typewriter;

import java.util.Optional;
import models.Model.ModelType;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

/**
 * ProgramOptions enumerates the options available from the command line, and
 * provides methods for parsing the command line.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public enum ProgramOptions {
  /** ANCHOR gives the first character of the alphabet. */
  ANCHOR,
  /** DATA is the path to the data file. */
  DATA,
  /** DISTANCECUTS determines whether to add redundant constraints on the
   * sum of distances from any symbol to all other symbols. */
  DISTANCECUTS,
  /** EMPHASIS is the MIP emphasis to be used by CPLEX. */
  EMPHASIS,
  /** HELP requests a usage message. */
  HELP,
  /** LAZY decides whether to make certain constraints lazy. */
  LAZY,
  /** LOG sets the log frequency for CPLEX models and the log verbosity
   * for CP Optimizer models. */
  LOG,
  /** MODEL selects the model to be run. */
  MODEL,
  /** PRIORITY determines whether to assign branching priorities. */
  PRIORITY,
  /** RPCUT decides whether to add an extra cut suggested by Rob Pratt. */
  RPCUT,
  /** SOS decides whether to add SOS1 constraints by position. */
  SOS,
  /** TIMELIMIT is the time limit (in seconds) for the solver. */
  TIMELIMIT,
  /** VERIFY specifies a candidate solution to be tested and, if verified as
   * feasible, to be used as an initial incumbent solution in optimization
   * models. */
  VERIFY;

  /**
   * Parse the command line into a vector of options.
   * @param args the command line arguments
   * @return the various options in an object vector
   * @throws ParseException if the command line cannot be parsed
   */
  public static Object[] parseCommandLine(final String[] args)
                         throws ParseException {
    // Allocate the return vector.
    Object[] vals = new Object[ProgramOptions.values().length];
    // Create the command line parser.
    Options opts = buildCLI();
    CommandLine cli = (new DefaultParser()).parse(opts, args);
    // See if the user needs help. If so, print the help message, fill in the
    // help option only and return the vector.
    vals[HELP.ordinal()] = cli.hasOption("h");
    if ((boolean) vals[HELP.ordinal()]) {
      printHelp(opts);
      return vals;
    }
    // Get the optional anchor value.
    Optional<Character> anchor = Optional.empty();
    if (cli.hasOption("a")) {
      anchor = Optional.of(cli.getOptionValue("a").charAt(0));
    }
    vals[ANCHOR.ordinal()] = anchor;
    // Get the data file. Throw an exception if it is not specified.
    if (cli.hasOption("d")) {
      vals[DATA.ordinal()] = cli.getOptionValue("d");
    } else {
      throw new ParseException("Required data option is missing.");
    }
    // Add redundant constraints on the sum of the distances from any symbol
    // to all other positions?
    vals[DISTANCECUTS.ordinal()] = cli.hasOption("dc");
    // Get the MIP emphasis if present.
    Optional<Integer> emphasis = Optional.empty();
    if (cli.hasOption("e")) {
      emphasis = Optional.of(Integer.parseInt(cli.getOptionValue("e")));
    }
    vals[EMPHASIS.ordinal()] = emphasis;
    // Get the log setting if present.
    Optional<Integer> log = Optional.empty();
    if (cli.hasOption("l")) {
      log = Optional.of(Integer.parseInt(cli.getOptionValue("l")));
    }
    vals[LOG.ordinal()] = log;
    // Determine which model to run.
    ModelType model = ModelType.MIP1;  // default
    if (cli.hasOption("m")) {
      model = ModelType.valueOf(cli.getOptionValue("m"));
    }
    vals[MODEL.ordinal()] = model;
    // Get the choice whether to assign branching priorities.
    vals[PRIORITY.ordinal()] = cli.hasOption("p");
    // Get the choice whether to use Rob's cut.
    vals[RPCUT.ordinal()] = cli.hasOption("r");
    // Get the choice whether to use SOS1 constraints.
    vals[SOS.ordinal()] = cli.hasOption("sp");
    // Get the time limit (if specified).
    Optional<Double> tl = Optional.empty();
    if (cli.hasOption("t")) {
      tl = Optional.of(Double.valueOf(cli.getOptionValue("t")));
    }
    vals[TIMELIMIT.ordinal()] = tl;
    // See if the user specified a solution to verify.
    Optional<String> start = Optional.empty();
    if (cli.hasOption("v")) {
      start = Optional.of(cli.getOptionValue("v"));
    }
    vals[VERIFY.ordinal()] = start;
    // Get the decision whether to make some constraints lazy.
    vals[LAZY.ordinal()] = cli.hasOption("z");
    // MIP3 does not allow branching priorities.
    if (vals[MODEL.ordinal()] == ModelType.MIP3) {
      vals[PRIORITY.ordinal()] = false;
    }
    return vals;
  }

  /**
   * Build the command line interface.
   * @return an Options argument defining the CLI
   */
  private static Options buildCLI() {
    Options cli = new Options();
    // Specify the character mapping to position 0 in the alphabet.
    cli.addOption(
      Option.builder("a")
            .longOpt("anchor")
            .hasArg()
            .desc("first character of the alphabet (optional)")
            .build()
    );
    // Specify the location of the data file.
    cli.addOption(
      Option.builder("d")
            .longOpt("data")
            .hasArg()
            .desc("path to the data file (required)")
            .build()
    );
    // Add redundant constraints on the sum of the distances from any symbol
    // to all other positions?
    cli.addOption(
      Option.builder("dc")
            .longOpt("distances")
            .desc("use optional constraints on sums of distances")
            .build()
    );
    // Set the CPLEX emphasis parameter.
    cli.addOption(
      Option.builder("e")
            .longOpt("emphasis")
            .hasArg()
            .desc("set the MIP emphasis")
            .build()
    );
    // Get help.
    cli.addOption(
      Option.builder("h")
            .longOpt("help")
            .desc("show this help message")
            .build()
    );
    // For the CP model, set the log verbosity. For MIP models, set the
    // log interval.
    cli.addOption(
      Option.builder("l")
            .longOpt("log")
            .hasArg()
            .desc("log frequency or verbosity (integer)")
            .build()
    );
    // Select which MIP model to run.
    cli.addOption(
      Option.builder("m")
            .longOpt("model")
            .hasArg()
            .desc("which model to run (MIP1, MIP2, MIP3, MIP4, QP or CP;"
                  + " default MIP1)")
            .build()
    );
    // Assign branching priorities based on frequency and keyboard position.
    cli.addOption(
      Option.builder("p")
            .longOpt("priority")
            .desc("assign branching priorities based on frequency")
            .build()
    );
    // Allow the user to employ Rob Pratt's suggested cut (in models that use
    // explicit distance variables).
    cli.addOption(
      Option.builder("r")
            .longOpt("robpratt")
            .desc("add a cut suggested by Rob Pratt")
            .build()
    );
    // Use SOS1 constraints by keyboard position.
    cli.addOption(
      Option.builder("sp")
            .longOpt("sos_pos")
            .desc("add SOS1 constraints by position")
            .build()
    );
    // Set a time limit for the solver.
    cli.addOption(
      Option.builder("t")
            .longOpt("timelimit")
            .hasArg()
            .desc("solver time limit (seconds)")
            .build()
    );
    // Verify a proposed solution.
    cli.addOption(
      Option.builder("v")
            .longOpt("verify")
            .hasArg()
            .desc("verify a solution (sequence of integers or letters,"
                  + " separated by commas and/or spaces and surrounded by"
                  + " quotes)")
            .build()
    );
    // Use lazy constraints where relevant.
    cli.addOption(
      Option.builder("z")
            .longOpt("lazy")
            .desc("use lazy constraints if relevant")
            .build()
    );
    return cli;
  }

  /**
   * Print a help message.
   * @param opts the command line options
   */
  private static void printHelp(final Options opts) {
    (new HelpFormatter()).printHelp("java -jar Typewriter.jar ", opts);
  }
}

