package typewriter;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.IntSummaryStatistics;
import java.util.Optional;
import java.util.Scanner;
import static typewriter.ProgramOptions.ANCHOR;
import static typewriter.ProgramOptions.DATA;

/**
 * Alphabet encapsulates the character set to be assigned to the keyboard.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class Alphabet {
  private final int size;           // number of characters in the alphabet
  private final Optional<Character> anchor;
                                    // first character of the alphabet
  private final int[][] frequency;  // frequency matrix
  private final int[] usage;        // how often characters appear in pairs
  private final int mostFrequent;   // index of the most frequent character

  /**
   * Constructor.
   * @param opts the option vector from the command line
   * @throws FileNotFoundException if the path to the data file is invalid
   */
  public Alphabet(final Object[] opts)
         throws FileNotFoundException {
    anchor = (Optional<Character>) opts[ANCHOR.ordinal()];
    // Read and parse the input file.
    String filespec = (String) opts[DATA.ordinal()];
    try (
      Scanner scanner = new Scanner(new File(filespec))) {
      size = scanner.nextInt();
      frequency = new int[size][size];
      for (int i = 0; i < size; i++) {
        for (int j = 0; j < size; j++) {
          frequency[i][j] = scanner.nextInt();
        }
      }
    }
    // Compute how often each symbol appears as one of a pair.
    usage = new int[size];
    for (int i = 0; i < size; i++) {
      int total = Arrays.stream(frequency[i]).sum(); // row sum
      for (int j = 0; j < size; j++) {
        total += frequency[j][i];                    // column sum
      }
      usage[i] = total;
    }
    // Find the most frequently used character.
    int max = -1;
    int ix = 0;
    for (int i = 0; i < size; i++) {
      if (usage[i] > max) {
        max = usage[i];
        ix = i;
      }
    }
    mostFrequent = ix;
  }

  /**
   * Convert a position (integer between 0 and size - 1) to a character.
   * @param pos the position
   * @return the corresponding character
   */
  public char toChar(final int pos) {
    if (anchor.isPresent()) {
      return (char) (anchor.get() + pos);
    } else {
      throw new UnsupportedOperationException("Trying to get a character"
                                              + " with no anchor present!");
    }
  }

  /**
   * Convert a string to the index in the alphabet of its first character.
   * @param symbol the symbol to convert
   * @return its index
   */
  public int toIndex(final String symbol) {
    int offset = (anchor.isPresent()) ? anchor.get() : 0;
    return symbol.charAt(0) - offset;
  }

  /**
   * Print out the data.
   * @return a string describing the data
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("The alphabet contains ").append(size).append(" characters ");
    if (anchor.isPresent()) {
      sb.append("from ").append(toChar(0)).append(" to ")
        .append(toChar(size - 1)).append(".\n");
    } else {
      sb.append("designated by integers 0 to ").append(size - 1).append(".\n");
    }
    sb.append("The most frequently encountered character is ");
    int x = mostFrequent();
    if (anchor.isPresent()) {
      sb.append(toChar(x));
    } else {
      sb.append(x);
    }
    sb.append(".\n\nThe frequency matrix is as follows:\n");
    for (int i = 0; i < size; i++) {
      for (int j = 0; j < size; j++) {
        sb.append(" ").append(frequency[i][j]);
      }
      sb.append("\n-----\n");
    }
    return sb.toString();
  }

  /**
   * Get the alphabet size.
   * @return the alphabet size
   */
  public int getSize() {
    return size;
  }

  /**
   * Is the alphabet represented by characters?
   * @return true if the alphabet is character-based (i.e., an anchor
   * is defined)
   */
  public boolean isCharacter() {
    return anchor.isPresent();
  }

  /**
   * Get the frequency with which one character follows another.
   * @param first the index of the first character
   * @param second the index of the following character
   * @return the frequency with which the second character follows the first
   */
  public int getFrequency(final int first, final int second) {
    return frequency[first][second];
  }

  /**
   * Find the index of the most frequently encountered character.
   * @return the index of the most frequently encountered character
   */
  public int mostFrequent() {
    return mostFrequent;
  }

  /**
   * Evaluate the objective function on a given layout.
   * Validate the solution first and throw a checked exception if it is not
   * valid.
   * @param layout the layout (a permutation of 0 ... size - 1)
   * @return the frequency-weighted sum of distances
   * @throws InvalidSolutionException if the layout is not valid
   */
  public long valueOf(final int[] layout) throws InvalidSolutionException {
    // Make sure all character indices are in the range 0 to (size - 1).
    IntSummaryStatistics stats = Arrays.stream(layout).summaryStatistics();
    int lo = stats.getMin();
    int hi = stats.getMax();
    if (lo != 0) {
      throw new InvalidSolutionException("Invalid starting index " + lo + ".");
    } else if (hi != size - 1) {
      throw new InvalidSolutionException("Invalid ending index " + hi + ".");
    }
    // Now verify that every index from 0 to (size - 1) occurs exactly once,
    // by verifying that the correct number of values were encountered and the
    // layout has the correct dimension.
    if (layout.length != size) {
      throw new InvalidSolutionException("The layout has incorrect length "
                                         + layout.length + ".");
    }
    boolean[] found = new boolean[size];
    Arrays.stream(layout).forEach((i) -> found[i] = true);
    for (int i = 0; i < size; i++) {
      if (!found[i]) {
        throw new InvalidSolutionException("Character index " + i
                                           + " does not appear in the layout.");
      }
    }
    // The layout is valid; compute its weighted cost.
    long total = 0;
    for (int i = 0; i < size; i++) {
      int a = layout[i];
      for (int j = i + 1; j < size; j++) {
        int b = layout[j];
        int dist = Math.abs(i - j);
        total += dist * (frequency[a][b] + frequency[b][a]);
      }
    }
    return total;
  }

  /**
   * Get the cumulative frequency of occurrence of a character (the frequency
   * with which it is either the first or second character in a pair of
   * consecutive characters).
   * @param index the index of the character
   * @return the frequency with which the character appears
   */
  public int cumulativeFrequency(final int index) {
    return usage[index];
  }

  /**
   * Get the usage figures for every symbol.
   * @return the usage values
   */
  public int[] usages() {
    return Arrays.copyOf(usage, size);
  }

  /**
   * Compute the sum of all distances between pairs of positions where the
   * first position is to the left o the second position.
   * This quantity is used in a cut suggested by Rob Pratt in a blog comment.
   * @return the sum of all distances d_{i,j} where i < j
   */
  @SuppressWarnings({ "checkstyle:magicnumber" })
  public double distanceSum() {
    return (Math.pow(size, 3) - size) / 6;
  }

  /**
   * Calculate the sum of the distances of all other positions from a specified
   * position.
   * @param pos the target position
   * @return the sum of distances to all other positions
   */
  public int sumDistances(final int pos) {
    return (pos * (pos + 1) + (size - pos - 1) * (size - pos)) / 2;
  }

  /**
   * Get the transition frequencies from a particular symbol.
   * @param symbol the starting symbol
   * @return the transition frequencies from that symbol
   */
  public int[] getFrequencies(final int symbol) {
    return frequency[symbol];
  }

  /**
   * Get the frequencies with which all other symbols follow a given symbol,
   * sorted into descending order.
   * @param symbol the target symbol
   * @return the sorted frequencies of all other symbols
   */
  public int[] getSortedFrequencies(final int symbol) {
    ArrayList<Integer> f = new ArrayList<>();
    for (int i = 0; i < size; i++) {
      if (i != symbol) {
        f.add(frequency[symbol][i]);
      }
    }
    return f.stream()
            .sorted(Comparator.reverseOrder())
            .mapToInt((i) -> i)
            .toArray();
  }

}
