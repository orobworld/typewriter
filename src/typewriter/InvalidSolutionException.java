package typewriter;

/**
 * InvalidSolutionException is an exception signaling that a keyboard layout
 * is not a valid sequence of the alphabet of characters.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class InvalidSolutionException extends Exception {

  /**
   * Constructor.
   * @param message an optional message
   */
  public InvalidSolutionException(final String message) {
    super(message);
  }

}
